#!/bin/sh

TOREMOVE=$(python -c 'print(u"\u00a0E".encode("utf8"))')

mkdir -p org/

for i in html/*.html
do
    cat $i | sed 's/&nbsp;/ /g' | pandoc -f html -t org | tail -n+2 \
	    | sed 's/\xC2\xA0//g' | tr '\n' ' ' \
	    | sed 's/\* \*/ /g' | sed 's/\*\*//g' | sed 's/\*\/\/\*/*/g' \
	    | sed 's/\* \/courbélant\/ \*/ \/courbélant\/ /g' > org/$(basename $i .html).org
done
