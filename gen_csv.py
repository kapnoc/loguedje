#!/usr/bin/env python3

import os
import csv

dico_dir = "./dico/org/"
out_file_name = "dictionary.curated.csv"

filenames = os.listdir(dico_dir)
filenames.sort()

with open(out_file_name, mode='w') as out_file:
    out_writer = csv.writer(out_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in filenames:
        print("=================================", i)
        contents = open(dico_dir + i).read().strip(" \n*").replace("\n", " ")
        sp = filter(lambda x: (len(x) != 0), map(str.strip ,contents.split("*")))
        it = iter(sp)
        for fr in it:
            vo = ""
            try:
                vo = next(it)
            except:
                print("LAST FR WITHOUT VO: %s" % fr)
                break
            if fr[-2:] != " :":
                print(fr, vo)
            else:
                desc = ""
                fr = fr[:-2]
                out_writer.writerow([fr, vo, desc])
